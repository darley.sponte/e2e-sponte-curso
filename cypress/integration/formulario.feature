describe('Meu primeiro teste', () => {
  it('Vá até o meu sistema', () => {
    cy.visit('http://localhost:3000')
  })

  it('Validar campos obrigatórios', () => {
    cy.get('[data-test=salvar]').click()
    cy.get('[data-test=error-nome]').contains('Campo obrigatório!')
  })

  it('Salvar formulário', () => {
    cy.get('[data-test=nome] input').type('Português').should('have.value', 'Português')

    cy.get('[data-test=descricao] input')
      .type('Disciplina de português')
      .should('have.value', 'Disciplina de português')

    cy.get('[data-test=salvar]').click()

    cy.get('[data-test=snackbar]').should('have.class', 'MuiAlert-filledSuccess')
  })

  it('Limpar formulário', () => {
    cy.get('[data-test=nome] input').clear()

    cy.get('[data-test=descricao] input').clear()
  })
})
