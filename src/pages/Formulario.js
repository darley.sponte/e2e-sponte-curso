import React, { memo, useCallback, useState } from 'react'

import { Container, Paper, Box, TextField, Button, Typography, Snackbar } from '@material-ui/core'

import MuiAlert from '@material-ui/lab/Alert'

const initialValues = { nome: '', descricao: '' }

export const Formulario = memo(() => {
  const [form, setForm] = useState(initialValues)

  const [errors, setErrors] = useState(initialValues)

  const [snackbar, setSnackbar] = useState({ type: 'success', onOpen: false, data: '' })

  const handleSubmit = useCallback(
    (e) => {
      e.preventDefault()

      const { nome } = form

      if (!nome) {
        setErrors({ nome: 'Campo obrigatório!' })

        setSnackbar({ type: 'error', onOpen: true, data: 'Campo obrigatório - Nome' })

        return
      }

      setSnackbar({ type: 'success', onOpen: true, data: JSON.stringify(form) })
    },
    [form]
  )

  const handleInput = useCallback(
    ({ target: { name, value } }) => {
      setErrors({ ...errors, [name]: '' })
      setForm({ ...form, [name]: value })
    },
    [errors, form]
  )

  return (
    <Container>
      <Paper>
        <Box m={5} p={5}>
          <form onSubmit={handleSubmit}>
            <Box mb={2}>
              <TextField
                id="standard-basic"
                label="Nome"
                data-test="nome"
                name="nome"
                fullWidth
                pb={5}
                onChange={handleInput}
              />
              <Typography variant="caption" display="block" color="error" gutterBottom data-test="error-nome">
                {errors.nome}
              </Typography>
            </Box>

            <Box mb={2}>
              <TextField
                id="standard-basic"
                label="Descrição"
                data-test="descricao"
                name="descricao"
                fullWidth
                pb={5}
                onChange={handleInput}
              />
            </Box>

            <Box display="flex" justifyContent="flex-end">
              <Button variant="contained" color="primary" type="submit" data-test="salvar">
                Salvar
              </Button>
            </Box>
          </form>
        </Box>
      </Paper>
      <Snackbar
        open={snackbar.onOpen}
        autoHideDuration={15000}
        onClose={() => setSnackbar({ ...snackbar, onOpen: false })}
      >
        <MuiAlert
          elevation={6}
          variant="filled"
          onClose={() => setSnackbar({ ...snackbar, onOpen: false })}
          severity={snackbar.type}
          data-test="snackbar"
        >
          {snackbar.data}
        </MuiAlert>
      </Snackbar>
    </Container>
  )
})
