import React from 'react'

import { Formulario } from './pages/Formulario'

function App() {
  return <Formulario />
}

export default App
